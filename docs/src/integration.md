# Chatbot Integration Document

## Overview

This document provides the necessary details for integrating a chatbot feature that sends user-generated questions to a specified endpoint. This integration enables the chatbot to communicate with an external service for processing or storing user queries. Additionally, the chatbot can now be integrated directly into websites through an iframe, offering a more seamless user experience.

## Iframe Integration

**Updated Integration Instructions:**

Integrating the chatbot into your website can now be easily achieved using an iframe, along with the necessary styles and scripts. This method allows you to embed the chatbot in a designated area of your webpage, providing a seamless interface for user interaction.

**Iframe Code:**

```html
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Chatbot Integration</title>
    <style>
        .chatbot-iframe {
            position: fixed;
            bottom: 0px;
            right: 0px;
            width: 100px;
            height: 100px;
            border: none;
            transition-delay: 0.8s;
            z-index: 9999
        }
        .chatbot-iframe.open {
            width: 430px;
            height: 630px;
            transition-delay:0s 
        }
        @media only screen and (max-width: 768px) {
            .chatbot-iframe.open {
                width: 100dvw;
                height: 100dvh;
                right: 0;
                bottom: 0;
            }
        }
    </style>
</head>
<body>
    <iframe
        src="https://d30catpr6gfaep.cloudfront.net"
        class="chatbot-iframe"
        id="chatbot-iframe"
        frameborder="0"
        scrolling="no">
    </iframe>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const cb = document.querySelector('.chatbot-iframe');
            if (!cb) {
                console.error('Chatbot iframe not found.');
                return;
            }
            // Detekcija jezika iz URL-a
            const currentUrl = window.location.href;
            let detectedLang = 'sr';  // Podrazumevani jezik je 'sr'
            if (currentUrl.includes('/en/')) {
                detectedLang = 'en';
            } else if (currentUrl.includes('/zh-hans/')) {
                detectedLang = 'zh-hans';
            } else if (currentUrl.includes('/de/')) {
                detectedLang = 'de';
            } else if (currentUrl.includes('/ru/')) {
                detectedLang = 'ru';
            } else if (currentUrl.includes('/sr/')) {
                detectedLang = 'sr';
            }
            const chatbotUrl = `${cb.src}?lang=${detectedLang}`;
            cb.src =chatbotUrl;
            
          setTimeout(() => {cb.contentWindow.postMessage({
                      screenWidth: window.innerWidth,
                      screenHeight: window.innerHeight
                    }, "*");}, 250);
      
            window.addEventListener('message', (event) => {
                if (event.data === 'closeChatbot') {
                    cb.classList.remove('open');
                } else if (event.data === 'openChatbot') {
                    cb.classList.add('open');
                    
                }
            });
        });
    </script>
</body>
</html>
```

**Integration Instructions:**

1. Copy the provided iframe code.
2. Paste it into the HTML of your webpage where you want the chatbot to appear.
3. Ensure that the styles and the script are included in your HTML.

The chatbot will then be readily accessible to your users, embedded directly on the page..

## Mobile App Integration

Integrating the chatbot into mobile applications on Android and iOS platforms can enhance the user experience by providing direct access to the chatbot functionalities within the app. Below are instructions for embedding the chatbot using a WebView in Android and a WKWebView in iOS.

### Android Integration (Kotlin)

1. **Add a WebView to your Activity**

   Include a WebView in your activity's layout XML file. For example:

   ```xml
   <WebView
       android:id="@+id/chatbotWebView"
       android:layout_width="match_parent"
       android:layout_height="match_parent"/>
   ```

2. **Configure the WebView**

   In your activity (e.g., `MainActivity.kt`), configure the WebView to load the chatbot iframe.

   ```kotlin
   import android.os.Bundle
   import android.webkit.WebView
   import androidx.appcompat.app.AppCompatActivity

   class MainActivity : AppCompatActivity() {

       override fun onCreate(savedInstanceState: Bundle?) {
           super.onCreate(savedInstanceState)
           setContentView(R.layout.activity_main)

           val chatbotWebView: WebView = findViewById(R.id.chatbotWebView)
           chatbotWebView.settings.javaScriptEnabled = true

           val iframeUrl = "https://d30catpr6gfaep.cloudfront.net?device=mobile"
           chatbotWebView.loadUrl(iframeUrl)
       }
   }
   ```

   Ensure you have the Internet permission in your `AndroidManifest.xml`.
3. **Internet Permission**

   Add the following line to your `AndroidManifest.xml`:

   ```xml
   <uses-permission android:name="android.permission.INTERNET"/>
   ```

### iOS Integration (Swift)

1. **Import WebKit**

   Ensure you have imported WebKit in your ViewController.

   ```swift
   import UIKit
   import WebKit
   ```

2. **Add a WKWebView to your ViewController**

   You can add a WKWebView programmatically or via the Interface Builder. Here's how to add it programmatically:

   ```swift
   var webView: WKWebView!

   override func viewDidLoad() {
       super.viewDidLoad()

       let webConfiguration = WKWebViewConfiguration()
       webView = WKWebView(frame: .zero, configuration: webConfiguration)
       webView.translatesAutoresizingMaskIntoConstraints = false
       view.addSubview(webView)

       NSLayoutConstraint.activate([
           webView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
           webView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
           webView.topAnchor.constraint(equalTo: view.topAnchor),
           webView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
       ])

       let myURL = URL(string:"https://d30catpr6gfaep.cloudfront.net?device=mobile")
       let myRequest = URLRequest(url: myURL!)
       webView.load(myRequest)
   }
   ```

### Security Considerations

When embedding external content in your mobile application, it's essential to consider security implications. Ensure the URL loaded in the WebView or WKWebView is secure. Use HTTPS wherever possible and implement appropriate content security policies.

## Testing and Validation

* Perform thorough testing to ensure the chatbot correctly formats and sends the user's question.
* Validate the integration by checking the responses from the endpoint and ensuring they align with expected outcomes.
* Test error handling to confirm the system's resilience in adverse scenarios.

## Support

For any questions or issues related to integrating the chatbot, whether through direct API calls or iframe embedding, please open an [issue](https://gitlab.com/labsoft-ai/atlas24-serverless-rag-documentation/-/issues) in the repository, or contact [support@labsoft.dev](mailto:support@labsoft.dev?subject=Support).
