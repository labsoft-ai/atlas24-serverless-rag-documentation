# Atlas24h Roadside Assistance AI Chatbot Documentation

Welcome to the official repository for the Atlas24h Roadside Assistance AI Chatbot documentation. This repository contains all the source files used to generate the Sphinx documentation available at [Atlas24h Documentation](https://labsoft-documentation.gitlab.io/atlas24-serverless-rag-documentation/). The documentation provides detailed information on integrating the Atlas24h AI Chatbot into various platforms, including web pages via iframe and mobile apps on Android and iOS.

## Repository Structure

Below is the folder tree structure of this repository, providing a clear overview of its organization:

```bash
.
├── config.json
├── docs
│   ├── conf.py
│   ├── index.rst
│   ├── make.bat
│   ├── Makefile
│   ├── prepare_plantuml.sh
│   ├── requirements.txt
│   ├── resources
│   │   ├── LabSoft2.png
│   │   └── LabSoft.png
│   └── src
│       └── integration.md
└── README.md
```

- `config.json` - Configuration file for project settings.
- `docs/` - Contains the Sphinx source files for the documentation.
  - `conf.py` - Sphinx configuration file.
  - `index.rst` - The main entry point for the Sphinx documentation.
  - `make.bat` and `Makefile` - Scripts for building the documentation on Windows and Unix-like systems, respectively.
  - `prepare_plantuml.sh` - Script to prepare PlantUML diagrams if used in the documentation.
  - `requirements.txt` - Python requirements for building the documentation.
  - `resources/` - Contains images and other resources used in the documentation.
  - `src/` - Markdown source files for the API documentation and integration guides.
- `README.md` - This file, providing an overview and instructions for the repository.

## Building Documentation Locally Using Docker

For those who prefer to use Docker for building the documentation, you can easily do so by using the "authsec/sphinx" Docker image. This method encapsulates the build environment and dependencies, ensuring a consistent build process.

### Prerequisites

- Docker installed on your system.

### Steps

1. **Pull the Docker Image**

   First, pull the "authsec/sphinx" Docker image from Docker Hub:

   ```bash
   docker pull authsec/sphinx
   ```

2. **Run Sphinx Build Command in Docker Container**

   Navigate to the root directory of this repository where the `docs/` folder is located. Then, run the following command to build the Sphinx documentation using the Docker container:

   ```bash
   docker run --rm -v $(pwd):/docs authsec/sphinx sphinx-build -b html docs public
   ```

   This command mounts the current directory (`$(pwd)`) to the `/docs` directory inside the container. The `sphinx-build -b html docs public` command is then executed within the container, generating the HTML documentation in the `public/` directory on your host machine.

### Output

The HTML documentation will be generated and stored in the `public/` directory within your project. You can open the `index.html` file in a web browser to view the documentation locally.

## Contributing

We welcome contributions to the Atlas24h AI Chatbot documentation. To contribute, please follow the steps outlined in the **Contributing** section above.

## Support

For questions, issues, or support related to the Atlas24h AI Chatbot documentation, please open an issue in this repository or contact [support@labsoft.dev](mailto:support@labsoft.dev?subject=Support).

Thank you for contributing to the Atlas24h AI Chatbot documentation!
