.. Atlas24 Serverless RAG documentation master file, created by
   sphinx-quickstart on Fri Dec 15 12:29:13 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Atlas24 Serverless RAG documentation!
=================================================

Contents
--------

.. toctree::
   :maxdepth: 2

   src/integration.md

