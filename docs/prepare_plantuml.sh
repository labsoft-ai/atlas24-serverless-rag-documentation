#!/bin/bash

# Replace "```plantuml" with "```{uml}" in all Markdown files in the specified directory
# Usage: ./prepare_plantuml.sh <directory_path>

DIRECTORY=$1

if [ -z "$DIRECTORY" ]; then
    echo "Directory not specified. Usage: ./prepare_plantuml.sh <directory_path>"
    exit 1
fi

if [ ! -d "$DIRECTORY" ]; then
    echo "Directory does not exist: $DIRECTORY"
    exit 1
fi

# Loop through all Markdown (.md) files in the specified directory
find "$DIRECTORY" -name '*.md' | while read -r file; do
    echo "Processing $file"
    sed -i 's/```plantuml/```{uml}/g' "$file"
done

echo "Replacement complete."
